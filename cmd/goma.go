// goma provides a command line tool to access materials databases
// in YAML format as used by yamdb.py
//
// https://stackoverflow.com/questions/38310894/golang-nested-yaml-values
package main

import (
	"embed"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"text/tabwriter" // printPropertyValue

	// . "github.com/logrusorgru/aurora" // colors
	ydb "goma/internal/yamdb"
	"gopkg.in/yaml.v2"
)

var (
	pversion string
	build    string
	branch   string
	commit   string
)

//go:embed data/*.yml
var fs embed.FS

func main() {

	var (
		calcAll                  bool
		calcDensity              bool
		calcDynamicViscosity     bool
		calcExpansionCoefficient bool
		calcHeatCapacity         bool
		calcResistivity          bool
		calcSoundVelocity        bool
		calcSurfaceTension       bool
		calcThermalConductivity  bool
		calcVapourPressure       bool
		calcVolumeChangeFusion   bool
		showTm                   bool
		showTb                   bool
		showM                    bool
		showHelp                 bool
		calcTemperature          string
		calcSubstance            string
		calcComposition          string
		calcDatabase             string
		dbYaml                   []byte
		ListSDB                  string
		Version                  bool
		Quiet                    bool
		Verbose                  bool
		Mixture                  bool
		err                      error
	)

	flag.BoolVar(&calcAll, "a", false, "all properties")
	flag.BoolVar(&calcAll, "all", false, "all properties")
	flag.BoolVar(&calcDensity, "d", false, "density")
	flag.BoolVar(&calcDensity, "density", false, "density")
	flag.BoolVar(&calcDynamicViscosity, "y", false, "dynamic viscosity")
	flag.BoolVar(&calcDynamicViscosity, "dynamic_viscosity", false, "dynamic viscosity")
	flag.BoolVar(&calcExpansionCoefficient, "e", false, "expansion coefficient")
	flag.BoolVar(&calcExpansionCoefficient, "expansion_coefficient", false, "expansion coefficient")
	flag.BoolVar(&showHelp, "h", false, "help")
	flag.BoolVar(&showHelp, "help", false, "help")
	flag.BoolVar(&calcHeatCapacity, "j", false, "heat capacity")
	flag.BoolVar(&calcHeatCapacity, "heat_capacity", false, "heat capacity")
	flag.BoolVar(&calcResistivity, "r", false, "electrical resistivity")
	flag.BoolVar(&calcResistivity, "resistivity", false, "electrical resistivity")
	flag.BoolVar(&calcSoundVelocity, "s", false, "sound velocity")
	flag.BoolVar(&calcSoundVelocity, "sound_velocity", false, "sound velocity")
	flag.BoolVar(&calcSurfaceTension, "u", false, "surface tension")
	flag.BoolVar(&calcSurfaceTension, "surface_tension", false, "surface tension")
	flag.BoolVar(&calcThermalConductivity, "t", false, "thermal conductivity")
	flag.BoolVar(&calcThermalConductivity, "thermal_conductivity", false, "thermal conductivity")
	flag.BoolVar(&calcVapourPressure, "p", false, "vapour pressure")
	flag.BoolVar(&calcVapourPressure, "vapour_pressure", false, "vapour pressure")
	flag.BoolVar(&calcVolumeChangeFusion, "i", false, "volume change on fusion")
	flag.BoolVar(&calcVolumeChangeFusion, "volume_change_fusion", false, "volume change on fusion")
	flag.BoolVar(&showTm, "m", false, "melting temperature")
	flag.BoolVar(&showTm, "Tm", false, "melting temperature")
	flag.BoolVar(&showTb, "b", false, "boiling temperature")
	flag.BoolVar(&showTb, "Tb", false, "boiling temperature")
	flag.BoolVar(&showM, "M", false, "molar mass")
	flag.BoolVar(&showM, "molar_mass", false, "molar mass")
	flag.StringVar(&calcTemperature, "T", "Tm", "temperature in K")
	flag.StringVar(&calcTemperature, "temperature", "Tm", "temperature in K")
	flag.StringVar(&calcSubstance, "S", "None", "substance")
	flag.StringVar(&calcSubstance, "substance", "None", "substance")
	flag.StringVar(&calcComposition, "c", "None", "composition")
	flag.StringVar(&calcComposition, "composition", "None", "composition")
	flag.StringVar(&calcDatabase, "f", "None", "substance database file")
	flag.StringVar(&calcDatabase, "file", "None", "substance database file")
	flag.StringVar(&ListSDB, "L", "None", "list substance database:\nA ... all substances, all properties\nS ... substances only\nP ... properties for selected substance\n")
	flag.StringVar(&ListSDB, "list", "None", "list substance database:\nall ... all substances, all properties\nsubstances ... substances only\nproperties ... properties for selected substance\n")
	flag.BoolVar(&Verbose, "v", false, "increase verbosity")
	flag.BoolVar(&Verbose, "verbose", false, "increase verbosity")
	flag.BoolVar(&Version, "V", false, "show version")
	flag.BoolVar(&Version, "version", false, "show version")
	flag.BoolVar(&Quiet, "q", false, "reduce verbosity")
	flag.BoolVar(&Quiet, "quiet", false, "reduce verbosity")

	flag.Parse()

	if Version {
		fmt.Fprintf(os.Stderr, "%s\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "version: %v\n", pversion)
		fmt.Fprintf(os.Stderr, "build: %v\n", build)
		fmt.Fprintf(os.Stderr, "branch %v, commit %v\n", branch, commit)
		os.Exit(0)
	}

	if showHelp {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(0)
	}

	if calcAll {
		calcDensity = true
		calcDynamicViscosity = true
		calcExpansionCoefficient = true
		calcHeatCapacity = true
		calcResistivity = true
		calcSoundVelocity = true
		calcSurfaceTension = true
		calcThermalConductivity = true
		calcVapourPressure = true
		calcVolumeChangeFusion = true
	}

	units := ydb.GetUnits()

	dbFile := calcDatabase

	Substance := calcSubstance

	Composition := calcComposition

	Mixture = false

	if ListSDB == "properties" {
		ListSDB = "P"
	}

	if Substance == "None" && (ListSDB == "None" || ListSDB == "P") {
		fmt.Fprintf(os.Stderr, "No substance specified!\n\n")
		os.Exit(1)
	}

	if strings.Contains(Substance, "-") {
		Mixture = true
		if Composition == "None" && ListSDB != "P" {
			fmt.Fprintf(os.Stderr, "Substance is a mixture but no composition specified!\nUse goma -S substance -c list to list available compositions\n")
			os.Exit(1)
		}
	}

	internalDBFiles := []string{"data/metals.yml",
		"data/Janz1992_salts.yml"}

	sdb := make(map[interface{}]interface{})

	if dbFile != "None" {
		_, err := os.Stat(dbFile)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Cannot access file %s!\n", dbFile)
			os.Exit(1)
		}
		dbYaml, err = ioutil.ReadFile(dbFile)
		if err != nil {
			panic(fmt.Errorf("error reading yaml file: %v", err))
		}
		if err := yaml.Unmarshal([]byte(dbYaml), &sdb); err != nil {
			panic(fmt.Errorf("error parsing yaml file: %v", err))
		}
	} else {
		// loop over all available embedded databases and search for Substance
		for _, file := range internalDBFiles {
			dbYaml, err = fs.ReadFile(file)
			if err != nil {
				panic(fmt.Errorf("%s was not found (%w)", file, err))
			}
			if err := yaml.Unmarshal([]byte(dbYaml), &sdb); err != nil {
				panic(fmt.Errorf("error parsing yaml file: %v", err))
			}
			if sdb[Substance] != nil {
				break
			}
		}
	}

	key := Substance

	if Mixture && Composition == "list" && sdb[key] != nil {
		ydb.ListCompositions(sdb, key)
		os.Exit(0)
	}

	if ListSDB == "A" || ListSDB == "all" {
		ListSDB = "A"
		ydb.ListSDB(sdb, "A", Substance)
		os.Exit(0)
	} else if ListSDB == "S" || ListSDB == "substances" {
		ListSDB = "S"
		ydb.ListSDB(sdb, "S", Substance)
		os.Exit(0)
	} else if ListSDB == "P" {
		if sdb[key] == nil {
			fmt.Fprintf(os.Stderr, "No information on substance: %v!\n\n", key)
		} else {
			ydb.ListSDB(sdb, "P", Substance)
		}
		os.Exit(0)
	}

	if sdb[key] == nil {
		fmt.Fprintf(os.Stderr, "No information on substance: %v!\n\n", key)
		os.Exit(1)
	}
	sm := make(map[interface{}]interface{})
	if Mixture && Composition != "None" {
		compositions := sdb[key].(map[interface{}]interface{})
		if compositions[Composition] == nil {
			fmt.Fprintf(os.Stderr, "No information on composition: %v!\n\n", Composition)
			os.Exit(1)
		}
		sm = compositions[Composition].(map[interface{}]interface{})
	} else {
		sm = sdb[key].(map[interface{}]interface{})
	}
	if calcDensity || calcDynamicViscosity || calcExpansionCoefficient || calcHeatCapacity || calcResistivity || calcSoundVelocity || calcSurfaceTension || calcThermalConductivity || calcVapourPressure || calcVolumeChangeFusion {
		basicProps := ydb.GetBasicProperties(sm, Verbose)
		Temperature := basicProps.Tm
		tw := new(tabwriter.Writer).Init(os.Stdout, 0, 8, 1, ' ', 0)
		// fmt.Fprintf(tw, "%v @ %v K (%v): %v %v\n", property, Temperature, equation, value, unit[property])
		if !Quiet {
			fmt.Fprintf(tw, "property\tvalue\tunit\tTemp\tTrange\tsource\n")
			fmt.Fprintf(tw, "\t\t\tK\t     K\t\n")
			fmt.Fprintf(tw, "---------\t-----\t----\t----\t------\t--------\n")
		}
		if calcTemperature != "Tm" {
			Temperature, err = strconv.ParseFloat(calcTemperature, 64)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Could not parse temperature. Please use an int or float value.\n\n")
				os.Exit(1)
			}
		} else {
			if Temperature <= 1.0e-06 { // practically zero
				fmt.Fprintf(os.Stderr, "Melting temperature not available, please specify a temperature using -T.\n\n")
				os.Exit(1)
			}
		}
		if calcDensity {
			printPropertyValue(tw, "density", key, sm, units, basicProps, Temperature, Verbose)
		}
		if calcDynamicViscosity {
			printPropertyValue(tw, "dynamic_viscosity", key, sm, units, basicProps, Temperature, Verbose)
		}
		if calcExpansionCoefficient {
			printPropertyValue(tw, "expansion_coefficient", key, sm, units, basicProps, Temperature, Verbose)
		}
		if calcHeatCapacity {
			printPropertyValue(tw, "heat_capacity", key, sm, units, basicProps, Temperature, Verbose)
		}
		if calcResistivity {
			printPropertyValue(tw, "resistivity", key, sm, units, basicProps, Temperature, Verbose)
		}
		if calcSoundVelocity {
			printPropertyValue(tw, "sound_velocity", key, sm, units, basicProps, Temperature, Verbose)
		}
		if calcSurfaceTension {
			printPropertyValue(tw, "surface_tension", key, sm, units, basicProps, Temperature, Verbose)
		}
		if calcThermalConductivity {
			printPropertyValue(tw, "thermal_conductivity", key, sm, units, basicProps, Temperature, Verbose)
		}
		if calcVapourPressure {
			printPropertyValue(tw, "vapour_pressure", key, sm, units, basicProps, Temperature, Verbose)
		}
		if calcVolumeChangeFusion {
			printPropertyValue(tw, "volume_change_fusion", key, sm, units, basicProps, Temperature, Verbose)
		}

		if !Quiet {
			fmt.Fprintf(tw,  "---------\t-----\t----\t----\t------\t--------\n")
		}
		tw.Flush()
	}
	if showTm {
		basicProps := ydb.GetBasicProperties(sm, Verbose)
		if !Quiet {
			fmt.Printf("Tm(%s): %v %s\n", key, basicProps.Tm, units["Tm"])
		} else {
			fmt.Printf("%v\n", basicProps.Tm)
		}
	}
	if showTb {
		basicProps := ydb.GetBasicProperties(sm, Verbose)
		if !Quiet {
			fmt.Printf("Tb(%s): %v %s\n", key, basicProps.Tb, units["Tb"])
		} else {
			fmt.Printf("%v\n", basicProps.Tb)
		}
	}
	if showM {
		basicProps := ydb.GetBasicProperties(sm, Verbose)
		if !Quiet {
			fmt.Printf("M(%s): %v %s\n", key, basicProps.M, units["M"])
		} else {
			fmt.Printf("%v\n", basicProps.M)
		}
	}
}

func selectEquation(property string, eqn map[interface{}]interface{}, basicProps ydb.BasicProperties, Temp float64) (float64, error) {
	var value float64
	var err error

	switch property {
	case "density":
		value, err = ydb.DensityEquation(eqn, basicProps, Temp)
	case "dynamic_viscosity":
		value, err = ydb.DynamicViscosityEquation(eqn, basicProps, Temp)
	case "expansion_coefficient":
		value, err = ydb.ExpansionCoefficientEquation(eqn, basicProps, Temp)
	case "heat_capacity":
		value, err = ydb.HeatCapacityEquation(eqn, basicProps, Temp)
	case "resistivity":
		value, err = ydb.ResistivityEquation(eqn, basicProps, Temp)
	case "sound_velocity":
		value, err = ydb.SoundVelocityEquation(eqn, basicProps, Temp)
	case "surface_tension":
		value, err = ydb.SurfaceTensionEquation(eqn, basicProps, Temp)
	case "thermal_conductivity":
		value, err = ydb.ThermalConductivityEquation(eqn, basicProps, Temp)
	case "vapour_pressure":
		value, err = ydb.VapourPressureEquation(eqn, basicProps, Temp)
	case "volume_change_fusion":
		value, err = ydb.VolumeChangeFusionEquation(eqn, basicProps, Temp)
	default:
		err = fmt.Errorf("No information on %q name", property)
	}
	return value, err
}

func printPropertyValue(tw io.Writer, property string, substance string, substanceMap map[interface{}]interface{}, unit map[string]string, basicProps ydb.BasicProperties, Temperature float64, Verbose bool) {
	var Tmin, Tmax float64
	var err error
	if substanceMap[property] == nil {
		if Verbose {
			fmt.Printf("No %v information for %v\n", property, substance)
		}
	} else {
		method := substanceMap[property].(map[interface{}]interface{})
		for source, v := range method {
			params := v.(map[interface{}]interface{})
			// equation := params["equation"]
			if params["reference"] != nil {
				source = params["reference"]
			}
			if params["Tmin"] != nil {
				Tmin, err = ydb.GetFloat(params["Tmin"])
				if err != nil && Verbose {
					fmt.Fprintf(os.Stderr, "Could not parse Tmin value, error: %v\n", err)
				}
			} else {
				Tmin = 0.0
			}
			if params["Tmax"] != nil {
				Tmax, err = ydb.GetFloat(params["Tmax"])
				if err != nil && Verbose {
					fmt.Fprintf(os.Stderr, "Could not parse Tmax value, error: %v\n", err)
				}
			} else {
				Tmax = 0.0
			}
			// reference := ydb.GetReference(params, source)
			value, err := selectEquation(property, params, basicProps, Temperature)
			if err != nil {
				if Verbose {
					fmt.Fprintf(os.Stderr, "%s %s: %v\n", property, source, err)
				}
			} else {
				// fmt.Fprintf(tw, "property\tTemp\tsource\tvalue\tunit\n")
				if property == "volume_change_fusion" {
					if basicProps.Tm >= 1.0e-06 { // practically zero
						fmt.Fprintf(tw, "%v\t%.5g\t%v\t%v\t    -\t%v\n", property, value, unit[property], basicProps.Tm, source)
					} else {
						fmt.Fprintf(tw, "%v\t%.5g\t%v\t%v\t    -\t%v\n", property, value, unit[property], "", source)
					}
				} else {
					fmt.Fprintf(tw, "%v\t%.5g\t%v\t%v", property, value, unit[property], Temperature)
					if Tmin > 0.0 {
						fmt.Fprintf(tw, "\t%3.0f-", Tmin)
					} else {
						fmt.Fprintf(tw, "\t    -")
					}
					if Tmax > 0.0 {
						fmt.Fprintf(tw, "%3.0f\t", Tmax)
					} else {
						fmt.Fprintf(tw, "\t")
					}
					fmt.Fprintf(tw, "%v\n", source)
				}
			}
		}
	}
}
