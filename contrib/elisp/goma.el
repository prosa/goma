;;; elisp snippet to interact with goma from emacs
;;;
;;; offers tab completion for materials, compositions and properties
;;;
;;; tested with emacs version 29.1 - 29.4
;;;
;;; Usage:
;;;
;;; simply copy into your ~/.emacs.d/init.el and make sure that
;;; the directory containing goma is in the path known to emacs
;;; (see below)
;;;
;;; bind to a key if you like, e.g.
;;; (global-set-key (kbd "C-c g") 'tw/goma)
;;;
;;; call with
;;; C-c g ;; if defined
;;; or
;;; M-x tw/goma
;;;
;;; material: TAB + select
;;; composition: TAB + select (if applicable)
;;; property: TAB + select ("all" is always a safe bet)
;;; temperature: value in K (if applicable, not needed for Tm, Tb, M)
;;;
;;; if goma is not found by emacs but can be used from the shell:
;;;
;;; Steve Purcell's version of exec-path-from-shell
;;;
;;; https://github.com/purcell/exec-path-from-shell
;;;
;;; is very helpful

(defun tw/goma ()
  "Interact with Goma to insert material properties."
  (interactive)
  (save-excursion)
  (let (material
	materials
	(all-properties '("Tb" "Tm" "all" "density" "dynamic_viscosity" "expansion_coefficient" "heat_capacity" "molar_mass" "resistivity" "sound_velocity" "surface_tension" "thermal_conductivity" "vapour_pressure" "volume_change_fusion"))
	(additional-properties '("all" "M" "molar_mass"))
	properties
	(composition "100-0")
	compositions
	(temperature 273.15)
	property
	properties-tmp)
    (catch 'break
    (progn
      (setq materials  (cdr ( cdr (split-string (setq materials (shell-command-to-string "goma -L S 2>/dev/null"))))))
      (setq material (completing-read "material: " materials))
      (if (not (member material materials))
	  (throw 'break (message "No information on material \"%s\" available!" material)))
      (if (string-search "-" material)
	  (progn
	    (setq compositions (split-string (shell-command-to-string (format "goma -S %s -c list 2>/dev/null" material))))
	    (setq composition (completing-read "composition: " compositions))
	    (if (not (member composition compositions))
		(throw 'break (message "No information on composition \"%s\" available!" composition)))))
      ;; get properties available for chosen material including a lot of noise (sources etc)
      (setq properties-tmp  (split-string (shell-command-to-string (format "goma -L P -S %s -c %s 2>/dev/null" material composition))))
      ;; if mixture, extract properties available for specified composition
      (if (string-search "-" material)
	  (setq properties-tmp (seq-take-while (lambda (elt) (not (string-search "-" elt))) (cdr (member composition properties-tmp)))))
      ;; fill a list with properties that are in all-properties _and_ in properties-tmp
      (setq properties '())
      (dolist (val properties-tmp) (if (member-ignore-case val all-properties) (add-to-list 'properties val)))
      (dolist (val additional-properties) (add-to-list 'properties val))
      ;;
      (setq property (completing-read "property: " properties))
      (if (not (member property properties))
	  (throw 'break (message "No information on property \"%s\" available!" property)))
      (if (not
	   (member-ignore-case property '("Tm" "m" "Tb" "b" "molar_mass" "volume_change_fusion")))
	  (setq temperature (read-string "temperature [K]: ")))
      (if (string= property "all")
	  (shell-command (format "goma -T %s -S %s -c %s -%s -Tm -Tb -M 2>/dev/null" temperature material composition property))
	(shell-command (format "goma -T %s -S %s -c %s -%s 2>/dev/null" temperature material composition property)))))))
