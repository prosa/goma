#!/usr/bin/env bash
#
# generate a properties table completed with a list of
# references from a goma run and with the help of pandoc
#
# adjust the variables below according to your needs/installation
#
GOMA="../../goma"
MATERIALSDB="None"
BIBLIOGRAPHY="../../data/references.bib"
EXT="pdf"  # or "docx", "html", "odt", "org", "tex", "txt"
OUTNAME="report"

ORG_RAW=$(mktemp -t org_raw.XXXXXXXXXX)
ORG_PROC=$(mktemp -t org_proc.XXXXXXXXXX)

NO_ARGS=0 
E_OPTERROR=85

if [ $# -eq "$NO_ARGS" ]    # Script invoked with no command-line args?
then
    echo "Usage: $(basename "$0") options (-B: -E: -G: -M: -O:)"
    echo ""
    echo "goma options need to be passed inside of a quoted string, e.g.,"
    echo ""
    echo "$(basename "$0") -G \"-S Na -T 800 -a\""
    echo ""
    echo "additional options:"
    echo "-B path_to_bibliography (default: ${BIBLIOGRAPHY})"
    echo "-E file extension (default: ${EXT}, possible values: \"docx\", \"html\", \"odt\", \"org\", \"pdf\", \"tex\", \"txt\")"
    echo "-M path_to_materials_database (default: ${MATERIALSDB})"
    echo "-O output_filename_without_extension (default: ${OUTNAME})"
    exit $E_OPTERROR
fi  

while getopts ":B:E:G:M:O:" Option
do
    case $Option in
	B ) BIBLIOGRAPHY=$OPTARG;;
	E ) EXT=$OPTARG;;
	G ) GOPTS=$OPTARG;;
	M ) MATERIALSDB=$OPTARG;;
	O ) OUTNAME=$OPTARG;;
	* ) echo "Unimplemented option chosen.";;   # Default.
    esac
done

FORMAT=${EXT}

if [ "${EXT}" == "txt" ]
then
    FORMAT="plain"
fi
if [ "${EXT}" == "tex" ]
then
    FORMAT="latex"
fi

if [ ! "${MATERIALSDB}" == "None" ]
then
    GOPTS="${GOPTS} -f ${MATERIALSDB}"
fi

# FIXME: regexp needs to be modified for compounds
# make sure that we end on an space for substance parsing
# echo "-S NaCl-CaCl2 -T 800"  |  perl -pe 's|-S (.*? ).*|\1|'
# https://stackoverflow.com/questions/1103149/non-greedy-reluctant-regex-matching-in-sed
SUBSTANCE=$(echo "${GOPTS} " | sed -n 's/.*-S \([^ ]*\) .*/\1/p')
if [[ ${SUBSTANCE} == *"-"* ]]
then
	COMPOSITION=$(echo "${GOPTS} " | sed -n 's/.*-c \([^ ]*\) .*/\1/p')
	SUBSTANCE=${SUBSTANCE}" "${COMPOSITION}
fi
# table with different widths in one column not 
# accepted/recognized by pandoc if not .org extension
{ echo "* Thermophysical properties of ${SUBSTANCE}";
  echo "| *property* | *value* | *unit* | *Temp* | *Trange* | *source* |";
  echo "|            |         |        |    *K* |      *K* |          |";
  echo "|            |         |        |    <r> |      <r> |          |";
  echo "|------------+---------+--------+--------+----------+----------|"; } > "${ORG_RAW}"
# FIXME: handle Tm, Tb, M properly (not just ignoring by "if (NF > 1)") if requested 
${GOMA} -q ${GOPTS} | \
	awk '{if (NF > 1) {print "|", $1, "|", $2, "|", $3, "|", $4, "|", $5, "|", "cite:"$6, "|"}}' \
	>> "${ORG_RAW}"
sed -e 's/dynamic_viscosity/dynamic\ viscosity/g'\
    -e 's/expansion_coefficient/expansion\ coefficient/g' \
    -e 's/heat_capacity/heat\ capacity/g' \
    -e 's/sound_velocity/sound velocity/g' \
    -e 's/surface_tension/surface tension/g' \
    -e 's/thermal_conductivity/thermal conductivity/g' \
    -e 's/vapour_pressure/vapour pressure/g' \
    -e 's/volume_change_fusion/volume change on fusion/g' \
    -e '/^| No.*/d' \
    < "${ORG_RAW}" > "${ORG_PROC}" 
echo "* References" >> "${ORG_PROC}"
# space in title suppresses pandoc warning for html output
pandoc "${ORG_PROC}" \
       --from org \
       --standalone \
       --metadata title=" " \
       -forg+smart \
       --citeproc \
       --csl eh_unsrt.csl \
       --bibliography="${BIBLIOGRAPHY}" \
       --pdf-engine=xelatex \
       -V geometry:a4paper \
       --to="${FORMAT}" \
       -o "${OUTNAME}"."${EXT}"
rm -f "${ORG_RAW}" "${ORG_PROC}"
