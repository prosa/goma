package yamdb

import (
	"errors"
	"fmt"
	"math"
)

func HeatCapacityGurvich1991(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var cp float64
	var err error

	cp_0, err1 := GetFloat(params["cp_0"])
	a, err2 := GetFloat(params["a"])
	b, err3 := GetFloat(params["b"])
	c, err4 := GetFloat(params["c"])
	d, err5 := GetFloat(params["d"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil || err5 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4, err5))
	} else {
		cp = (cp_0 + a*Temp + b*math.Pow(Temp, 2.0) +
			c*math.Pow(Temp, 3.0) + d/math.Pow(Temp, 2.0))
	}
	return cp, err
}

func HeatCapacityIAEA2008(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var cp float64
	var err error

	cp_0, err1 := GetFloat(params["cp_0"])
	a, err2 := GetFloat(params["a"])
	b, err3 := GetFloat(params["b"])
	c, err4 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		cp = cp_0 + a*Temp + b*math.Pow(Temp, 2.0) + c/Temp
		cp *= 1000.0 // kJ -> J
	}
	return cp, err
}

func HeatCapacityIidaGuthrie1988(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var cp float64

	cp_mol, err := GetFloat(params["cp_mol"])
	energy_unit, ok := params["energy_unit"].(string)
	if !ok {
		energy_unit = "J" // default unit, otherwise cal
	}
	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	} else {
		if energy_unit == "cal" {
			cp_mol *= 4.184 // cal -> J according to Janz (1979) p. 4
		}
		cp = cp_mol / bp.M // M assumed to be in kg/mol
	}
	return cp, err
}

func HeatCapacityIidaGuthrie2015(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var cp float64
	var err error
	energy_unit, ok := params["energy_unit"].(string)
	if !ok {
		energy_unit = "J" // default unit, otherwise cal
	}
	cp_0, err1 := GetFloat(params["cp_0"])
	a, err2 := GetFloat(params["a"])
	b, err3 := GetFloat(params["b"])
	c, err4 := GetFloat(params["c"])

	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		cp_mol := cp_0 + a*Temp + b*math.Pow(Temp, 2.0) + c*math.Pow(Temp, -2.0)
		if energy_unit == "cal" {
			cp_mol *= 4.184 // cal -> J according to Janz (1979) p. 4
		}
		cp = cp_mol / bp.M // M assumed to be in kg/mol
	}
	return cp, err
}

func HeatCapacityImbeni1998(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var cp float64
	var err error

	cp_0, err1 := GetFloat(params["cp_0"])
	a, err2 := GetFloat(params["a"])
	b, err3 := GetFloat(params["b"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		cp = cp_0 + a*Temp + b/math.Pow(Temp, 2.0)
	}
	return cp, err
}

func HeatCapacityOhse1985(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var cp float64
	var err error

	C1, err1 := GetFloat(params["C1"])
	C2, err2 := GetFloat(params["C2"])
	C3, err3 := GetFloat(params["C3"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		cp_mol := C1 + C2*Temp + C3*math.Pow(Temp, 2.0)
		cp = cp_mol / bp.M // M assumed to be in kg/mol
	}
	return cp, err
}

func HeatCapacitySobolev2011(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var cp float64
	var err error

	cp_0, err1 := GetFloat(params["cp_0"])
	a, err2 := GetFloat(params["a"])
	b, err3 := GetFloat(params["b"])
	c, err4 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		cp = cp_0 + a*Temp + b*math.Pow(Temp, 2.0) + c/math.Pow(Temp, 2.0)
	}
	return cp, err
}

func HeatCapacityEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var cp float64

	equation := eqn["equation"].(string)

	switch equation {
	case "Gurvich1991":
		cp, err = HeatCapacityGurvich1991(eqn, bp, Temp)
	case "IAEA2008":
		cp, err = HeatCapacityIAEA2008(eqn, bp, Temp)
	case "IidaGuthrie1988":
		cp, err = HeatCapacityIidaGuthrie1988(eqn, bp, Temp)
	case "IidaGuthrie2015":
		cp, err = HeatCapacityIidaGuthrie2015(eqn, bp, Temp)
	case "Imbeni1998":
		cp, err = HeatCapacityImbeni1998(eqn, bp, Temp)
	case "Ohse1985":
		cp, err = HeatCapacityOhse1985(eqn, bp, Temp)
	case "Sobolev2011":
		cp, err = HeatCapacitySobolev2011(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return cp, err
}
