package yamdb

import (
	"errors"
	"fmt"
	"math"
)

func DynamicViscosityAssaeletal2012(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	a1, err1 := GetFloat(params["a1"])
	a2, err2 := GetFloat(params["a2"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		eta = -a1 + a2/Temp
		eta = math.Pow(10.0, eta)
		eta /= 1000.0
	}
	return eta, err
}

func DynamicViscosityHirai1992(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	R := 8.3144 // J mol^-1 K^-1 (gas constant Hirai (1992))
	A, err1 := GetFloat(params["A"])
	B, err2 := GetFloat(params["B"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		eta = A * math.Exp(B*1000.0/(R*Temp)) // kJ -> J in B
		eta /= 1000.0                         // m Pa s -> Pa s
	}
	return eta, err
}

func DynamicViscosityIAEA2008(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		eta = a * math.Pow(Temp, b) * math.Exp(c/Temp)
	}
	return eta, err
}

func DynamicViscosityJanzetal1968(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err, err3, err4 error

	c := 0.0
	d := 0.0
	val, ok := params["c"]
	if ok {
		c, err3 = GetFloat(val)
	}
	val, ok = params["d"]
	if ok {
		d, err4 = GetFloat(val)
	}
	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		eta = a + b*Temp + c*math.Pow(Temp, 2) + d*math.Pow(Temp, 3)
		eta /= 1000.0 // m Pa s -> Pa s (cP -> Pa s)
	}
	return eta, err
}

func DynamicViscosityJanzetal1968exp(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	R := 1.98717 // cal mol^-1 deg^-1
	A, err1 := GetFloat(params["A"])
	E, err2 := GetFloat(params["E"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		eta = A * math.Exp(E/(R*Temp))
		eta /= 1000.0 // m Pa s -> Pa s
	}
	return eta, err
}

func DynamicViscosityKostalMalek2010(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	A, err1 := GetFloat(params["A"])
	B, err2 := GetFloat(params["B"])
	T0, err3 := GetFloat(params["T0"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		eta = A + B/(Temp-T0)
		eta = math.Exp(eta)
	}
	return eta, err // already in Pa s
}

func DynamicViscosityLinstrom1992plusE(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err, err3 error
	var R float64
	val, ok := params["R"]
	if ok {
		R, err3 = GetFloat(val)
	} else {
		R = 8.31441
	}
	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		eta = D1 * math.Exp(D2/(R*Temp))
		eta /= 1000.0 // m Pa s -> Pa s
	}
	return eta, err
}

func DynamicViscosityLinstrom1992DP(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64

	D1, err := GetFloat(params["D1"])
	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	} else {
		eta = D1 / 1000.0 // m Pa s -> Pa s
	}
	return eta, err
}

func DynamicViscosityLinstrom1992E1(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	R := 8.31441
	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		eta = D1*math.Exp(D2/(R*Temp)) + D3/math.Pow(Temp, 2)
		eta /= 1000.0 // m Pa s -> Pa s
	}
	return eta, err
}

func DynamicViscosityLinstrom1992E2(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	R := 8.31441
	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		eta = D1 * math.Exp(D2/(R*(Temp-D3)))
		eta /= 1000.0 // m Pa s -> Pa s
	}
	return eta, err
}

func DynamicViscosityLinstrom1992P2(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		eta = D1 + D2*Temp + D3*math.Pow(Temp, 2)
		eta /= 1000.0 // m Pa s -> Pa s
	}
	return eta, err
}

func DynamicViscosityLinstrom1992P3(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	D4, err4 := GetFloat(params["D4"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		eta = D1 + D2*Temp + D3*math.Pow(Temp, 2) + D4*math.Pow(Temp, 3)
		eta /= 1000.0 // m Pa s -> Pa s
	}
	return eta, err
}

func DynamicViscosityOhse1985(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		eta = a + b*math.Log(Temp) + c/Temp
		eta = math.Exp(eta)
	}
	return eta, err
}

func DynamicViscosityToerklepOeye1982(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var eta float64
	var err error
	
	A, err1 := GetFloat(params["A"])
	B, err2 := GetFloat(params["B"])
	C, err3 := GetFloat(params["C"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		eta = A * math.Exp(B/Temp+C/math.Pow(Temp, 2))
		eta /= 1000.0 // m Pa s -> Pa s
	}
	return eta, err
}

func DynamicViscosityEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var eta float64

	equation := eqn["equation"].(string)
	// fmt.Printf("%s\n", equation)

	switch equation {
	case "Assaeletal2012":
		eta, err = DynamicViscosityAssaeletal2012(eqn, bp, Temp)
	case "Hirai1992":
		eta, err = DynamicViscosityHirai1992(eqn, bp, Temp)
	case "IAEA2008":
		eta, err = DynamicViscosityIAEA2008(eqn, bp, Temp)
	case "Janzetal1968":
		eta, err = DynamicViscosityJanzetal1968(eqn, bp, Temp)
	case "Janzetal1968exp":
		eta, err = DynamicViscosityJanzetal1968exp(eqn, bp, Temp)
	case "KostalMalek2010":
		eta, err = DynamicViscosityKostalMalek2010(eqn, bp, Temp)
	case "Linstrom1992DP":
		eta, err = DynamicViscosityLinstrom1992DP(eqn, bp, Temp)
	case "Linstrom1992E1":
		eta, err = DynamicViscosityLinstrom1992E1(eqn, bp, Temp)
	case "Linstrom1992E2":
		eta, err = DynamicViscosityLinstrom1992E2(eqn, bp, Temp)
	case "Linstrom1992P2":
		eta, err = DynamicViscosityLinstrom1992P2(eqn, bp, Temp)
	case "Linstrom1992P3":
		eta, err = DynamicViscosityLinstrom1992P3(eqn, bp, Temp)
	case "Linstrom1992plusE":
		eta, err = DynamicViscosityLinstrom1992plusE(eqn, bp, Temp)
	case "Ohse1985":
		eta, err = DynamicViscosityOhse1985(eqn, bp, Temp)
	case "ToerklepOeye1982":
		eta, err = DynamicViscosityToerklepOeye1982(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return eta, err
}
