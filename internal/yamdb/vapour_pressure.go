package yamdb

import (
	"errors"
	"fmt"
	"math"
)

func VapourPressureIAEA2008(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var pv float64

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	d, err4 := GetFloat(params["d"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		pv = a + b/Temp + c*math.Log(Temp) + d*Temp
		pv = math.Exp(pv)
	}
	return pv, err
}

func VapourPressureIidaGuthrie2015(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var pv float64

	A, err1 := GetFloat(params["A"])
	B, err2 := GetFloat(params["B"])
	C, err3 := GetFloat(params["C"])
	unit, ok := params["unit"].(string)
	if !ok {
		unit = "atm" // default unit, otherwise mmHg or Pa
	}
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		pv = A + B/Temp + C*math.Log10(Temp)
		pv = math.Pow(10.0, pv)
		if unit == "mmHg" {
			pv *= 133.322 // mmHg -> Pa
		} else if unit == "Pa" {
			pv = pv
		} else {
			pv *= 101325.0 // atm -> Pa
		}
	}
	return pv, err
}

func VapourPressureKelley1935(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var pv float64

	A, err1 := GetFloat(params["A"])
	B, err2 := GetFloat(params["B"])
	C, err3 := GetFloat(params["C"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		R := 1.9869  // cal/K
		deltaF := A + B*Temp*math.Log10(Temp) - C*Temp
		pv = math.Exp(- deltaF/(R*Temp))
		pv *= 101325.0  // atm -> Pa
	}
	return pv, err
}

func VapourPressureOhse1985(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var pv float64

	A, err1 := GetFloat(params["A"])
	B, err2 := GetFloat(params["B"])
	C, err3 := GetFloat(params["C"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		pv = A + B/Temp + C*math.Log(Temp)
		pv = math.Exp(pv)
		pv *= 1.0e06 // MPa -> Pa
	}
	return pv, err
}

func VapourPressureSobolev2011(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var pv float64

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		pv = a * math.Exp(b/Temp)
	}
	return pv, err
}

func VapourPressureEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var pv float64

	equation := eqn["equation"].(string)

	switch equation {
	case "IidaGuthrie2015":
		pv, err = VapourPressureIidaGuthrie2015(eqn, bp, Temp)
	case "IAEA2008":
		pv, err = VapourPressureIAEA2008(eqn, bp, Temp)
	case "Kelley1935":
		pv, err = VapourPressureKelley1935(eqn, bp, Temp)
	case "Ohse1985":
		pv, err = VapourPressureOhse1985(eqn, bp, Temp)
	case "Sobolev2011":
		pv, err = VapourPressureSobolev2011(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return pv, err
}
