package yamdb

import (
	"errors"
	"fmt"
)

func SoundVelocityBlairs2007(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var v float64

	A, err1 := GetFloat(params["A"])
	B, err2 := GetFloat(params["B"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		v = A + B*Temp
	}
	return v, err
}

func SoundVelocityBlairs2007Cubic(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var v float64

	A, err1 := GetFloat(params["A"])
	B, err2 := GetFloat(params["B"])
	C, err3 := GetFloat(params["C"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		v = A + B*Temp + C*Temp*Temp
	}
	return v, err
}

func SoundVelocityIAEA2008(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var v float64

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		v = a + b/Temp + c*(Temp-273.15)
	}
	return v, err
}

func SoundVelocityLinearExperimental(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err, err3 error
	var v float64
	var Tm float64

	Um, err1 := GetFloat(params["Um"])
	m_dUdT, err2 := GetFloat(params["m_dUdT"])
	_, ok := params["Tm"]
	if !ok {
		Tm = bp.Tm
	} else {
		Tm, err3 = GetFloat(params["Tm"])
	}
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		v = Um - (Temp-Tm)*m_dUdT
	}
	return v, err
}

func SoundVelocityEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var v float64

	equation := eqn["equation"].(string)

	switch equation {
	case "Blairs2007":
		v, err = SoundVelocityBlairs2007(eqn, bp, Temp)
	case "Blairs2007cubic":
		v, err = SoundVelocityBlairs2007Cubic(eqn, bp, Temp)
	case "IAEA2008":
		v, err = SoundVelocityIAEA2008(eqn, bp, Temp)
	case "linearExperimental":
		v, err = SoundVelocityLinearExperimental(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return v, err
}
