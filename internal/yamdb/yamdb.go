package yamdb

import (
	"fmt"
	"io"
	"os" // listSDB
	"sort"
	"strconv"
	"strings"
	"text/tabwriter" // listSDB
)

func GetSortedKeys(list map[interface{}]interface{}) []string {
	// initialization with fixed length (p. 95) does not work, i.e.,
	// len(sdb) seems not to work
	var keys_sorted []string

	// p. 7: _ ... blank identifier necessary because
	// Go does not permit unused local variables, this would result in a
	// compilation error
	for key, _ := range list {
		keys_sorted = append(keys_sorted, key.(string))
	}

	sort.Strings(keys_sorted)

	return keys_sorted
}

func sortCompositions(compositions []string) ([]string, error) {
	var err error
	sort.Slice(compositions, func(i, j int) bool {
		var err_a, err_b error
		a_conc := strings.Split(compositions[i], "-")[0]
		b_conc := strings.Split(compositions[j], "-")[0]
		a, err_a := strconv.ParseFloat(a_conc, 64)
		if err_a != nil {
			err = fmt.Errorf("%v %v", err, err_a)
		}
		b, err_b := strconv.ParseFloat(b_conc, 64)
		if err_b != nil {
			err = fmt.Errorf("%v %v", err, err_b)
		}

		return a < b
	})
	return compositions, err
}

func GetSortedCompositions(list map[interface{}]interface{}) ([]string, error) {
	// initialization with fixed length (p. 95) does not work, i.e.,
	// len(sdb) seems not to work
	var keys_sorted []string
	var err error

	// p. 7: _ ... blank identifier necessary because
	// Go does not permit unused local variables, this would result in a
	// compilation error
	for key, _ := range list {
		if key != "range" {
			keys_sorted = append(keys_sorted, key.(string))
		}
	}

	keys_sorted, err = sortCompositions(keys_sorted)

	return keys_sorted, err
}

func GetReference(params map[interface{}]interface{}, source string) string {
	var reference string

	if params["reference"] == nil {
		reference = source
	} else {
		reference = fmt.Sprint(params["reference"])
	}

	return reference
}

func ListCompositions(sdb map[interface{}]interface{}, substance string) {
	compositions := sdb[substance].(map[interface{}]interface{})
	ks, err := GetSortedCompositions(compositions)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error sorting compositions: %v!\n\n", err)
		os.Exit(1)
	}
	for _, k := range ks {
		fmt.Printf("%v\n", k)
	}
}

func ListSDB(sdb map[interface{}]interface{}, what string, substance string) {
	// tabwriter does not play well together with colors
	// probably color codes spoil the tabwriter algorithm ...
	// https://stackoverflow.com/questions/35398497/how-do-i-get-colors-to-work-with-golang-tabwriter
	// see https://github.com/juju/ansiterm for an alternative
	tw := new(tabwriter.Writer).Init(os.Stdout, 0, 8, 1, ' ', 0)

	ks := GetSortedKeys(sdb)

	if what == "A" { // list everything
		fmt.Fprintf(tw, "substance\tcomposition\tproperty\tsource\t\n")
		fmt.Fprintf(tw, "---------\t-----------\t--------\t------\t\n")

		for _, k := range ks {
			fmt.Fprintf(tw, "%v\t\t\t\t\n", k)
			properties := sdb[k].(map[interface{}]interface{})

			if strings.Contains(k, "-") { // substance is mixture
				compositions := sdb[k].(map[interface{}]interface{})
				kc, err := GetSortedCompositions(compositions)

				if err != nil {
					fmt.Fprintf(os.Stderr, "Error sorting compositions: %v!\n\n", err)
					os.Exit(1)
				}

				for _, kci := range kc {
					fmt.Fprintf(tw, "\t%v\t\t\t\n", kci)
					composition := compositions[kci].(map[interface{}]interface{})
					kkcp := GetSortedKeys(composition)
					for _, kkcpi := range kkcp {
						if kkcpi == "M" || kkcpi == "Tb" || kkcpi == "Tm" {
							if composition[kkcpi] != nil {
								val, err := GetFloat(composition[kkcpi])
								if err == nil && val > 0.0 {
									fmt.Fprintf(tw, "\t\t%v\t\t\n", kkcpi)
								}
							}
						} else {
							composition_property := composition[kkcpi].(map[interface{}]interface{})
							printPropertyReference(tw, composition_property, kkcpi)
						}
					}
				}
			} else { // substance is pure (no mixture)
				kks := GetSortedKeys(properties)

				for _, kk := range kks {
					if kk == "M" || kk == "Tb" || kk == "Tm" {
						fmt.Fprintf(tw, "\t\t%v\t\t\n", kk)
					} else {
						sources := properties[kk].(map[interface{}]interface{})
						printPropertyReference(tw, sources, kk)
					}
				}
			}
		}
	}
	if what == "S" { // list substances only
		fmt.Fprintf(tw, "substance\t\n")
		fmt.Fprintf(tw, "---------\t\n")

		for _, k := range ks {
			fmt.Fprintf(tw, "%v\t\n", k)
		}
	}
	if what == "P" { // list properties for selected substance
		fmt.Fprintf(tw, "substance\tcomposition\tproperty\tsource\t\n")
		fmt.Fprintf(tw, "---------\t-----------\t--------\t------\t\n")
		fmt.Fprintf(tw, "%v\t\t\t\t\n", substance)
		properties := sdb[substance].(map[interface{}]interface{})

		kks := GetSortedKeys(properties)

		if strings.Contains(substance, "-") { // substance is mixture
			kc, err := GetSortedCompositions(properties)

			if err != nil {
				fmt.Fprintf(os.Stderr, "Error sorting compositions: %v!\n\n", err)
				os.Exit(1)
			}

			for _, kci := range kc {
				fmt.Fprintf(tw, "\t%v\t\t\t\n", kci)
				composition := properties[kci].(map[interface{}]interface{})
				kkcp := GetSortedKeys(composition)
				for _, kkcpi := range kkcp {
					if kkcpi == "M" || kkcpi == "Tb" || kkcpi == "Tm" {
						if composition[kkcpi] != nil {
							val, err := GetFloat(composition[kkcpi])
							if err == nil && val > 0.0 {
								fmt.Fprintf(tw, "\t\t%v\t\t\n", kkcpi)
							}
						}
					} else {
						composition_property := composition[kkcpi].(map[interface{}]interface{})
						printPropertyReference(tw, composition_property, kkcpi)
					}
				}
			}
		} else { // substance is pure (no mixture)
			for _, kk := range kks {
				if kk == "M" || kk == "Tb" || kk == "Tm" {
					fmt.Fprintf(tw, "\t\t%v\t\t\n", kk)
				} else {
					sources := properties[kk].(map[interface{}]interface{})
					printPropertyReference(tw, sources, kk)
				}
			}
		}
	}
	tw.Flush()
}

func GetFloat(unk interface{}) (float64, error) {
	var err error
	converted := 0.0
	valStr := fmt.Sprint(unk)
	if valStr != "<nil>" {
		converted, err = strconv.ParseFloat(valStr, 64)
	} else {
		err = fmt.Errorf("String is nil")
	}
	return converted, err
}

func GetUnits() map[string]string {
	units := map[string]string{
		"density":               "kg/m³",
		"dynamic_viscosity":     "Pa·s",
		"expansion_coefficient": "1/K",
		"heat_capacity":         "J/(kg·K)",
		"resistivity":           "Ω·m",
		"sound_velocity":        "m/s",
		"surface_tension":       "N/m",
		"thermal_conductivity":  "W/(m·K)",
		"vapour_pressure":       "Pa",
		"volume_change_fusion":  "%",
		"Tm":                    "K",
		"Tb":                    "K",
		"M":                     "kg/mol",
	}
	return units
}

type BasicProperties struct {
	Tm float64
	Tb float64
	M  float64
}

func GetBasicProperties(params map[interface{}]interface{}, Verbose bool) BasicProperties {
	var bp BasicProperties
	var err error

	bp.Tm, err = GetFloat(params["Tm"])
	if err != nil && Verbose {
		fmt.Fprintf(os.Stderr, "Could not parse Tm value, error: %v\n", err)
	}
	bp.Tb, err = GetFloat(params["Tb"])
	if err != nil && Verbose {
		fmt.Fprintf(os.Stderr, "Could not parse Tb value, error: %v\n", err)
	}
	bp.M, err = GetFloat(params["M"])
	if err != nil && Verbose {
		fmt.Fprintf(os.Stderr, "Could not parse M value, error: %v\n", err)
	}

	return bp
}

type Ohse1985Params struct {
	a    float64
	b    float64
	c    float64
	d    float64
	Tmin float64
	Tmax float64
}

func printPropertyReference(tw io.Writer, dict map[interface{}]interface{}, property string) {
	sources := GetSortedKeys(dict)
	for i, source := range sources {
		reference := GetReference(dict[source].(map[interface{}]interface{}), source)
		if i == 0 {
			fmt.Fprintf(tw, "\t\t%v\t%v\n", property, reference)
		} else {
			fmt.Fprintf(tw, "\t\t\t%v\n", reference)
		}
	}
}
