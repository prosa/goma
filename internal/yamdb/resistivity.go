package yamdb

import (
	"errors"
	"fmt"
	"math"
)

func ResistivityBaker1968(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho_e float64
	var err error

	A, err1 := GetFloat(params["A"])
	B, err2 := GetFloat(params["B"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		logrho_e := A/Temp - B
		rho_e = math.Pow(10.0, logrho_e)
		rho_e /= 100.0 // Ohm cm -> Ohm m
	}
	return rho_e, err
}

func ResistivityConstant(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	value, err := GetFloat(params["value"])
	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	}
	return value, err
}

func ResistivityCusackEnderby1960(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho_e float64
	var err error

	alpha, err1 := GetFloat(params["alpha"])
	beta, err2 := GetFloat(params["beta"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		rho_e = alpha*Temp + beta
	}
	return rho_e, err
}

func ResistivityDesaietal1984(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho_e float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	d, err4 := GetFloat(params["d"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		rho_e = a + b*Temp + c*math.Pow(Temp, 2.0) + d*math.Pow(Temp, 3.0)
		rho_e *= 1.0e-08
	}
	return rho_e, err
}

func ResistivityFractionalNegativeExponent(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho_e float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		rho_e = a + b/math.Pow(Temp, c)
	}
	return rho_e, err
}

func ResistivityIAEA2008(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho_e float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	d, err4 := GetFloat(params["d"])
	e, err5 := GetFloat(params["e"])
	f, err6 := GetFloat(params["f"])
	rho_e0, err7 := GetFloat(params["rho_e0"])
	t := Temp - 273.15
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil || err5 != nil || err6 != nil || err7 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4, err5, err6, err7))
	} else {
		rho_e = (a + b*t + c*math.Pow(t, 2.0) + d*math.Pow(t, 3.0) +
			e*math.Pow(t, 4.0) + f*math.Pow(t, 5.0))
		rho_e *= rho_e0
	}
	return rho_e, err
}

func ResistivityJanz1967exp(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err error

	R := 1.98717e-03 // kcal mol^-1 K^-1
	A, err1 := GetFloat(params["A"])
	E, err2 := GetFloat(params["E"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		kappa = A * math.Exp(-E/(R*Temp))
		kappa *= 100.0 // 1/(Ohm cm) -> 1/(Ohm m)
	}
	return 1.0 / kappa, err
}

func ResistivityJanzetal1968(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err, err3, err4, err5 error

	c := 0.0
	d := 0.0
	Tref := 0.0
	val, ok := params["c"]
	if ok {
		c, err3 = GetFloat(val)
	}
	val, ok = params["d"]
	if ok {
		d, err4 = GetFloat(val)
	}
	val, ok = params["Tref"]
	if ok {
		Tref, err5 = GetFloat(val)
	}
	
	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil || err5 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4, err5))
	} else {
		kappa = a + b*(Temp-Tref) + c*math.Pow(Temp-Tref, 2) + d*math.Pow(Temp-Tref, 3)
		kappa *= 100.0 // 1/(Ohm cm) -> 1/(Ohm m)
	}
	return 1.0 / kappa, err
}

func ResistivityLinstrom1992plusE(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err error

	R := 8.31441
	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		kappa = D1 * math.Exp(D2/(R*Temp))
		kappa *= 100.0 // 1/(Ohm cm) -> 1/(Ohm m)
	}
	return 1.0 / kappa, err
}

func ResistivityLinstrom1992DP(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64

	D1, err := GetFloat(params["D1"])
	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	} else {
		kappa = D1 * 100.0 // 1/(Ohm cm) -> 1/(Ohm m)
	}
	return 1.0 / kappa, err
}

func ResistivityLinstrom1992E2(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err error

	R := 8.31441
	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		kappa = D1 * math.Exp(D2/(R*(Temp-D3)))
		kappa *= 100.0 // 1/(Ohm cm) -> 1/(Ohm m)
	}
	return 1.0 / kappa, err
}

func ResistivityLinstrom1992P1(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		kappa = D1 + D2*Temp
		kappa *= 100.0 // 1/(Ohm cm) -> 1/(Ohm m)
	}
	return 1.0 / kappa, err
}

func ResistivityLinstrom1992P2(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		kappa = D1 + D2*Temp + D3*math.Pow(Temp, 2)
		kappa *= 100.0 // 1/(Ohm cm) -> 1/(Ohm m)
	}
	return 1.0 / kappa, err
}

func ResistivityLinstrom1992P3(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	D4, err4 := GetFloat(params["D4"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		kappa = D1 + D2*Temp + D3*math.Pow(Temp, 2) + D4*math.Pow(Temp, 3)
		kappa *= 100.0 // 1/(Ohm cm) -> 1/(Ohm m)
	}
	return 1.0 / kappa, err
}

func ResistivityLinstrom1992P4(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	D4, err4 := GetFloat(params["D4"])
	D5, err5 := GetFloat(params["D5"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil || err5 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4, err5))
	} else {
		kappa = D1 + D2*Temp + D3*math.Pow(Temp, 2) + D4*math.Pow(Temp, 3) + D5*math.Pow(Temp, 4)
		kappa *= 100.0 // 1/(Ohm cm) -> 1/(Ohm m)
	}
	return 1.0 / kappa, err
}

func ResistivityMassetetal2006(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err error

	A, err1 := GetFloat(params["A"])
	E, err2 := GetFloat(params["E"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		kappa = A * math.Exp(E/Temp)
		kappa *= 100.0 // Ohm cm -> Ohm m
	}
	return 1.0 / kappa, err
}

func resistivityOhse1985Func(T float64, a float64, b float64, c float64, d float64, Tmin float64) float64 {
	logT := math.Log10(T / Tmin)
	logrho_e := a + b*logT + c*math.Pow(logT, 2) + d*math.Pow(logT, 3)
	rho_e := math.Pow(10.0, logrho_e)
	return rho_e
}

func ResistivityOhse1985(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err, err1, err2, err3, err4, err5, err6 error
	rangeInterface := params["range"].([]interface{})
	var temp_p Ohse1985Params
	var rho_e float64
	calculated := false
	range_p := []Ohse1985Params{}
	for _, v := range rangeInterface {
		temp_p.a, err1 = GetFloat(v.(map[interface{}]interface{})["a"])
		temp_p.b, err2 = GetFloat(v.(map[interface{}]interface{})["b"])
		temp_p.c, err3 = GetFloat(v.(map[interface{}]interface{})["c"])
		temp_p.d, err4 = GetFloat(v.(map[interface{}]interface{})["d"])
		temp_p.Tmin, err5 = GetFloat(v.(map[interface{}]interface{})["Tmin"])
		temp_p.Tmax, err6 = GetFloat(v.(map[interface{}]interface{})["Tmax"])
		range_p = append(range_p, temp_p)
		err = errors.Join(err, err1, err2, err3, err4, err5, err6)
	}
	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	} else {
		for _, coeffs := range range_p {
			if Temp < coeffs.Tmax && Temp >= coeffs.Tmin {
				rho_e = resistivityOhse1985Func(Temp, coeffs.a, coeffs.b, coeffs.c, coeffs.d, coeffs.Tmin)
				calculated = true
			}
		}
		if calculated == false {
			err = fmt.Errorf("Ohse1985: Temperature %v out of range for resistivity", Temp)
		}
		rho_e *= 1e-08
	}
	return rho_e, err
}

func ResistivitySalyulevPotapov2015(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var kappa float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	d, err4 := GetFloat(params["d"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		logkappa := a + b/Temp + c/math.Pow(Temp, 2) + d/math.Pow(Temp, 3)
		kappa = math.Pow(10.0, logkappa)
		kappa *= 100.0 // S/cm -> S/m
	}
	return 1.0 / kappa, err
}

func ResistivitySobolev2011(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho_e float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		rho_e = a + b*Temp + c*Temp*Temp
	}
	return rho_e, err
}

func ResistivityZinkle1998(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho_e float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	d, err4 := GetFloat(params["d"])
	e, err5 := GetFloat(params["e"])
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil || err5 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4, err5))
	} else {
		rho_e = (a + b*Temp + c*math.Pow(Temp, 2.0) + d*math.Pow(Temp, 3.0) +
			e*math.Pow(Temp, 4.0))
		rho_e *= 1.e-09 // nano Ohm m
	}
	return rho_e, err
}

func ResistivityEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var rho_e float64

	equation := eqn["equation"].(string)

	switch equation {
	case "Baker1968":
		rho_e, err = ResistivityBaker1968(eqn, bp, Temp)
	case "constant":
		rho_e, err = ResistivityConstant(eqn, bp, Temp)
	case "CusackEnderby1960":
		rho_e, err = ResistivityCusackEnderby1960(eqn, bp, Temp)
	case "Desaietal1984":
		rho_e, err = ResistivityDesaietal1984(eqn, bp, Temp)
	case "fractionalNegativeExponent":
		rho_e, err = ResistivityFractionalNegativeExponent(eqn, bp, Temp)
	case "IAEA2008":
		rho_e, err = ResistivityIAEA2008(eqn, bp, Temp)
	case "Janz1967exp":
		rho_e, err = ResistivityJanz1967exp(eqn, bp, Temp)
	case "Janzetal1968":
		rho_e, err = ResistivityJanzetal1968(eqn, bp, Temp)
	case "Linstrom1992plusE":
		rho_e, err = ResistivityLinstrom1992plusE(eqn, bp, Temp)
	case "Linstrom1992DP":
		rho_e, err = ResistivityLinstrom1992DP(eqn, bp, Temp)
	case "Linstrom1992E2":
		rho_e, err = ResistivityLinstrom1992E2(eqn, bp, Temp)
	case "Linstrom1992P1":
		rho_e, err = ResistivityLinstrom1992P1(eqn, bp, Temp)
	case "Linstrom1992P2":
		rho_e, err = ResistivityLinstrom1992P2(eqn, bp, Temp)
	case "Linstrom1992P3":
		rho_e, err = ResistivityLinstrom1992P3(eqn, bp, Temp)
	case "Linstrom1992P4":
		rho_e, err = ResistivityLinstrom1992P4(eqn, bp, Temp)
	case "Massetetal2006":
		rho_e, err = ResistivityMassetetal2006(eqn, bp, Temp)
	case "Ohse1985":
		rho_e, err = ResistivityOhse1985(eqn, bp, Temp)
	case "SalyulevPotapov2015":
		rho_e, err = ResistivitySalyulevPotapov2015(eqn, bp, Temp)
	case "Sobolev2011":
		rho_e, err = ResistivitySobolev2011(eqn, bp, Temp)
	case "Zinkle1998":
		rho_e, err = ResistivityZinkle1998(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return rho_e, err
}
