package yamdb

import (
	"errors"
	"fmt"
	"math"
)

func DensityAssaeletal2012(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	c1, err1 := GetFloat(params["c1"])
	c2, err2 := GetFloat(params["c2"])
	Tref, err3 := GetFloat(params["Tref"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		rho = c1 - c2*(Temp-Tref)
	}
	return rho, err
}

func DensityBockrisetal1962(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	Tref, ok := params["Tref"].(float64)
	if !ok {
		Tref = 273.15
	}
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		rho = a - b*(Temp-Tref) - c*(Temp-Tref)*(Temp-Tref)
	}
	return rho, err
}

func DensityDoboszGancarz2018(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	a_1, err1 := GetFloat(params["a1"])
	a_2, err2 := GetFloat(params["a2"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		rho = a_1*Temp + a_2
	}
	return rho, err
}

func DensityIAEA2008(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	rho_0, err1 := GetFloat(params["rho_0"])
	a, err2 := GetFloat(params["a"])
	b, err3 := GetFloat(params["b"])
	c, err4 := GetFloat(params["c"])
	d, err5 := GetFloat(params["d"])
	e, err6 := GetFloat(params["e"])
	t := Temp - 273.15
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil || err5 != nil || err6 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4, err5, err6))
	} else {
		rho = rho_0 * (a + b*t + c*math.Pow(t, 2) +
			d*math.Pow(t, 3) + e*math.Pow(t, 3))
	}
	return rho, err
}

func DensityShpilrain1985(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	// https://stackoverflow.com/questions/37079018/how-convert-array-of-interface-to-float
	ainterface := params["a"].([]interface{})
	a := []float64{}

	for _, v := range ainterface {
		f, errv := GetFloat(v)
		a = append(a, f)
		err = errors.Join(err, errv)
	}

	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	} else {
		rho = 0.0
		tau := Temp / 1000.0
		for i, c := range a {
			rho += c * math.Pow(tau, float64(i))
		}
		rho *= 1000.0
	}
	return rho, err
}

func DensitySobolev2011(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	rho_s, err1 := GetFloat(params["rho_s"])
	drhodT, err2 := GetFloat(params["drhodT"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		rho = rho_s + drhodT*Temp
	}
	return rho, err
}

func DensitySteinberg1974(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	rho_m, err1 := GetFloat(params["rho_m"])
	lambda, err2 := GetFloat(params["lambda"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		rho = rho_m - lambda*(Temp-bp.Tm)
	}
	return rho, err
}

func DensityLinstrom1992DP(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	rho, err = GetFloat(params["D1"])
	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	} else {
		rho *= 1000.0 //  g/cm^3 -> kg/m^3
	}
	return rho, err
}

func DensityLinstrom1992P1(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		rho = D1 + D2*Temp
		rho *= 1000.0 //  g/cm^3 -> kg/m^3
	}
	return rho, err
}

func DensityLinstrom1992P2(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		rho = D1 + D2*Temp + D3*math.Pow(Temp, 2)
		rho *= 1000.0 //  g/cm^3 -> kg/m^3
	}
	return rho, err
}

func DensityEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var density float64

	equation := eqn["equation"].(string)

	switch equation {
	case "Assaeletal2012":
		density, err = DensityAssaeletal2012(eqn, bp, Temp)
	case "Bockrisetal1962":
		density, err = DensityBockrisetal1962(eqn, bp, Temp)
	case "DoboszGancarz2018":
		density, err = DensityDoboszGancarz2018(eqn, bp, Temp)
	case "IAEA2008":
		density, err = DensityIAEA2008(eqn, bp, Temp)
	case "Linstrom1992DP":
		density, err = DensityLinstrom1992DP(eqn, bp, Temp)
	case "Linstrom1992P1":
		density, err = DensityLinstrom1992P1(eqn, bp, Temp)
	case "Linstrom1992P2":
		density, err = DensityLinstrom1992P2(eqn, bp, Temp)
	case "Shpilrain1985":
		density, err = DensityShpilrain1985(eqn, bp, Temp)
	case "Sobolev2011":
		density, err = DensitySobolev2011(eqn, bp, Temp)
	case "Steinberg1974":
		density, err = DensitySteinberg1974(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return density, err
}
