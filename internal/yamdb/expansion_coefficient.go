package yamdb

import (
	"errors"
	"fmt"
	"math"
)

func ExpansionCoefficientIAEA2008(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var beta float64
	var err error

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	d, err4 := GetFloat(params["d"])
	t := Temp - 273.15
	if err1 != nil || err2 != nil || err3 != nil || err4 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3, err4))
	} else {
		beta = a + b*t + c*math.Pow(t, 2.0) + d*math.Pow(t, 3.0)
		beta /= 1.0e04
	}
	return beta, err
}

func ExpansionCoefficientOECDNEA2015(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var beta float64

	a, err := GetFloat(params["a"])
	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	} else {
		beta = 1.0 / (a - Temp)
	}
	return beta, err
}

func ExpansionCoefficientSobolev2011(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var rho float64
	var err error

	rho_s, err1 := GetFloat(params["rho_s"])
	drhodT, err2 := GetFloat(params["drhodT"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		rho = rho_s + drhodT*Temp
	}
	return -drhodT / rho, err
}

func ExpansionCoefficientSteinberg1974(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var beta float64
	var err error

	rho_m, err1 := GetFloat(params["rho_m"])
	lambda_, err2 := GetFloat(params["lambda"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		rho := rho_m - lambda_*(Temp-bp.Tm)
		beta = lambda_ / rho
	}
	return beta, err
}

func ExpansionCoefficientEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var beta float64

	equation := eqn["equation"].(string)

	switch equation {
	case "IAEA2008":
		beta, err = ExpansionCoefficientIAEA2008(eqn, bp, Temp)
	case "OECDNEA2015":
		beta, err = ExpansionCoefficientOECDNEA2015(eqn, bp, Temp)
	case "Sobolev2011":
		beta, err = ExpansionCoefficientSobolev2011(eqn, bp, Temp)
	case "Steinberg1974":
		beta, err = ExpansionCoefficientSteinberg1974(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return beta, err
}
