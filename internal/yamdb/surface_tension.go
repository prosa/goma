package yamdb

import (
	"errors"
	"fmt"
	"math"
)

func SurfaceTensionIAEA2008(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var sigma float64

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		sigma = a + b*(Temp+c)
		sigma /= 1000.0 // mN -> N
	}
	return sigma, err
}

func SurfaceTensionLinstrom1992DP(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var sigma float64

	D1, err := GetFloat(params["D1"])
	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	} else {
		sigma = D1 / 1000.0 // mN -> N
	}
	return sigma, err
}

func SurfaceTensionLinstrom1992P1(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var sigma float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		sigma = D1 + D2*Temp
		sigma /= 1000.0 // mN -> N
	}
	return sigma, err
}

func SurfaceTensionLinstrom1992P2(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var sigma float64
	var err error

	D1, err1 := GetFloat(params["D1"])
	D2, err2 := GetFloat(params["D2"])
	D3, err3 := GetFloat(params["D3"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		sigma = D1 + D2*Temp + D3*math.Pow(Temp, 2)
		sigma /= 1000.0 // mN -> N
	}
	return sigma, err
}

func SurfaceTensionSobolev2011(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var sigma float64

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		sigma = (a + b*Temp)
		sigma /= 1000. // mN -> N
	}
	return sigma, err
}

func SurfaceTensionEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var sigma float64

	equation := eqn["equation"].(string)

	switch equation {
	case "IAEA2008":
		sigma, err = SurfaceTensionIAEA2008(eqn, bp, Temp)
	case "linearExperimental":
		sigma, err = SurfaceTensionSobolev2011(eqn, bp, Temp)
	case "Linstrom1992DP":
		sigma, err = SurfaceTensionLinstrom1992DP(eqn, bp, Temp)
	case "Linstrom1992P1":
		sigma, err = SurfaceTensionLinstrom1992P1(eqn, bp, Temp)
	case "Linstrom1992P2":
		sigma, err = SurfaceTensionLinstrom1992P2(eqn, bp, Temp)
	case "Sobolev2011":
		sigma, err = SurfaceTensionSobolev2011(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return sigma, err
}
