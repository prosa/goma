package yamdb

import (
	"fmt"
)

func VolumeChangeFusionSchinkeSauerwald1956(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var dV float64
	var err error

	dV, err = GetFloat(params["dV"])
	if err != nil {
		err = fmt.Errorf("Parameter conversion error: %v", err)
	}
	return dV, err
}

func VolumeChangeFusionEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var dV float64

	equation := eqn["equation"].(string)

	switch equation {
	case "SchinkeSauerwald1956":
		dV, err = VolumeChangeFusionSchinkeSauerwald1956(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return dV, err
}
