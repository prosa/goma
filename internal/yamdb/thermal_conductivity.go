package yamdb

import (
	"errors"
	"fmt"
	"math"
)

func ThermalConductivityAssaeletal2017(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var lambda_ float64

	c1, err1 := GetFloat(params["c1"])
	c2, err2 := GetFloat(params["c2"])
	Tm, err3 := GetFloat(params["Tm"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		lambda_ = c1 + c2*(Temp-Tm)
	}
	return lambda_, err
}

func ThermalConductivityChliatzouetal2018(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err, err3 error
	var lambda_, Tm float64

	c0, err1 := GetFloat(params["c0"])
	c1, err2 := GetFloat(params["c1"])
	val, ok := params["Tm"]
	if ok {
		Tm, err3 = GetFloat(val)
	} else {
		Tm = bp.Tm
	}
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		lambda_ = c0 + c1*(Temp-Tm)
		lambda_ /= 1000.0 // mW/(m K) -> W/(m K)
	}
	return lambda_, err
}

func ThermalConductivityIAEA2008(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var lambda_ float64

	t := Temp - 273.15
	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		lambda_ = a + b*t + c*math.Pow(t, 2.0)
	}
	return lambda_, err
}

func ThermalConductivitySobolev2011(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var lambda_ float64

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		lambda_ = a + b*Temp + c*math.Pow(Temp, 2.0)
	}
	return lambda_, err
}

func ThermalConductivitySquareRoot(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var lambda_ float64

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	c, err3 := GetFloat(params["c"])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2, err3))
	} else {
		lambda_ = a + b*Temp + c*math.Pow(Temp, 0.5)
	}
	return lambda_, err
}

func ThermalConductivityTouloukian1970b(params map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var lambda_ float64

	a, err1 := GetFloat(params["a"])
	b, err2 := GetFloat(params["b"])
	if err1 != nil || err2 != nil {
		err = fmt.Errorf("Parameter conversion error: %v", errors.Join(err1, err2))
	} else {
		lambda_ = a + b*Temp
	}
	return lambda_, err
}

func ThermalConductivityEquation(eqn map[interface{}]interface{}, bp BasicProperties, Temp float64) (float64, error) {
	var err error
	var lambda_ float64

	equation := eqn["equation"].(string)

	switch equation {
	case "Assaeletal2017":
		lambda_, err = ThermalConductivityAssaeletal2017(eqn, bp, Temp)
	case "Chliatzouetal2018":
		lambda_, err = ThermalConductivityChliatzouetal2018(eqn, bp, Temp)
	case "IAEA2008":
		lambda_, err = ThermalConductivityIAEA2008(eqn, bp, Temp)
	case "Sobolev2011":
		lambda_, err = ThermalConductivitySobolev2011(eqn, bp, Temp)
	case "SquareRoot":
		lambda_, err = ThermalConductivitySquareRoot(eqn, bp, Temp)
	case "Touloukian1970b":
		lambda_, err = ThermalConductivityTouloukian1970b(eqn, bp, Temp)
	default:
		err = fmt.Errorf("invalid equation name %q", equation)
	}
	return lambda_, err
}
