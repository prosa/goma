# list of valid GOOS/GOARCH combinations with
# > go tool dist list
# RasPi4: GOARCH := "arm"
GOOS := "linux"
# GOOS := "windows"
GOARCH := "amd64"
# GOARCH := "arm64"
BUILD_DATE := $(shell date +%Y-%m-%d\ %H:%M)
PVERSION := "0.2.0"
INPUT = cmd/goma.go ./internal/yamdb/yamdb.go ./internal/yamdb/density.go ./internal/yamdb/dynamic_viscosity.go ./internal/yamdb/expansion_coefficient.go ./internal/yamdb/heat_capacity.go ./internal/yamdb/resistivity.go ./internal/yamdb/sound_velocity.go ./internal/yamdb/surface_tension.go ./internal/yamdb/thermal_conductivity.go ./internal/yamdb/vapour_pressure.go ./data/metals.yml ./data/Janz1992_salts.yml
GOMA := goma
ifeq (${GOOS}, "windows")
GOMA := goma.exe
endif

all: ${GOMA} pdf html

${GOMA}: $(INPUT)
	cp -R data cmd/
	GOOS=${GOOS} GOARCH=${GOARCH} go build -ldflags "-X 'main.pversion=${PVERSION}' -X 'main.build=${BUILD_DATE}'" -o ${GOMA} cmd/goma.go
	rm -rf cmd/data

pdf: docs/goma.pdf

html: docs/goma.html

docs/goma.pdf: docs/goma.1
	groff -mdoc -Tpdf -P-pa4 docs/goma.1  > docs/goma.pdf

docs/goma.html: docs/goma.1
	groff -mdoc -Thtml docs/goma.1  > docs/goma.html

install-user: docs/goma.1 ${GOMA} data/metals.yml data/Janz1992_salts.yml data/references.bib
	if [ ! -d ~/.local/bin ]; \
	then \
	mkdir -p ~/.local/bin/; \
	fi
	cp ${GOMA} ~/.local/bin/
	if [ ! -d ~/.local/share/man ]; \
	then \
	mkdir -p ~/.local/share/man/man1/; \
	fi
	cp docs/goma.1 ~/.local/share/man/man1
	if [ ! -d ~/.local/share/yamdb/ ]; \
	then \
	mkdir -p ~/.local/share/yamdb/; \
	fi
	cp data/metals.yml data/Janz1992_salts.yml data/references.bib ~/.local/share/yamdb
	@echo
	@echo "Locations of binary and man page should be in your path variables"
	@echo "for bash/zsh add"
	@echo
	@echo 'export PATH=$$PATH:~/.local/bin/'
	@echo 'export MANPATH=$$MANPATH:~/.local/share/man'
	@echo
	@echo "to your shell's init file"
clean:
	rm -f goma goma.exe *~ contrib/*~ \
	contrib/report_generator/properties-template.{bbl,pdf,tex} \
	contrib/report_generator/report.{aux,html,log,odt,org,pdf,tex,txt} \
	contrib/report_generator/*~
